﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /*---------------------
     * public fields
     * --------------------*/

    [SerializeField]
    Transform playerTransform;

    /*---------------------
     * private fields
     * --------------------*/

    Vector3 lastPlayerPosition;
    Vector3 newCameraPosition;
    Vector3 newPlayerPosition;

    const float cameraToPlayerOffsetY = 8.0f;
    const float cameraToPlayerOffsetZ = 8.0f;

    /*--------------------
     * private methods
     * -------------------*/

    void Start()
    {
        lastPlayerPosition = playerTransform.position;

        newCameraPosition = new Vector3(
            lastPlayerPosition.x,
            lastPlayerPosition.y + cameraToPlayerOffsetY,
            lastPlayerPosition.z - cameraToPlayerOffsetZ
        );

        transform.localPosition = newCameraPosition;
        transform.LookAt(playerTransform);
    }

    void Update()
    {
        newPlayerPosition = playerTransform.position;
        newCameraPosition = transform.position + (newPlayerPosition - lastPlayerPosition);

        transform.position = newCameraPosition;

        lastPlayerPosition = newPlayerPosition;
    }
}
