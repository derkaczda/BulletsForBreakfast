﻿/*---------------------------------------------------------
 *
 * BulletsForBreakfast
 * Player.cs
 *
 * Created: 05.05.2019
 * By: Daniel D.
 *
 * ---------------------------------------------------------*/

using UnityEngine;


public class Player : Character
{
    /*--------------------
     * public fields
     * -------------------*/

    [SerializeField]
    Transform bulletSpawnTransform;

    /*--------------------
     * private fields
     * -------------------*/
    #region private fields

    float currentShootingDelay = 0.0f;

    #endregion

    /*--------------------
     * protected methods
     * -------------------*/
    #region protected methods

    protected override void HandleMovement()
    {
        Vector3 objectLocalPosition = gameObject.GetComponent<Transform>().localPosition;
        float x = Input.GetAxisRaw("Joystick_Left_Horizontal");
        float y = Input.GetAxisRaw("Joystick_Left_Vertical");

        objectLocalPosition.x += MovementSpeed * Time.deltaTime * x;
        objectLocalPosition.z += MovementSpeed * Time.deltaTime * y;

        gameObject.GetComponent<Transform>().localPosition = objectLocalPosition;
    }

    protected override void HandleRotation()
    {
        float x = Input.GetAxisRaw("Joystick_Right_Horizontal");
        float y = Input.GetAxisRaw("Joystick_Right_Vertical");

        float joystickAngle = (Mathf.Atan2(y, x) * Mathf.Rad2Deg);
        float angle = 0.0f;
        angle = 90.0f - joystickAngle;
        if (x == 0.0f && y == 0.0f)
            angle = 0.0f;

        Vector3 currentRotation = transform.rotation.eulerAngles;
        currentRotation.y = angle * RotationSpeed;
        transform.eulerAngles = currentRotation;
    }

    protected override void HandleCharacterShoot()
    {
        if ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0) ||
            Input.GetAxis("Fire1") == 1.0f) && currentShootingDelay <= 0.0f)
        {
            ShootProjectile();
            currentShootingDelay = ShootingDelay;
        }
        currentShootingDelay -= Time.deltaTime;
    }

    private void ShootProjectile()
    {
        ProjectileController projController = SpawnProjectile();
        projController.shooterObject = gameObject;
        projController.lifeTime = 5.0f;
        projController.movingSpeed = 5.0f;
        projController.shootingDirection = bulletSpawnTransform.forward;
    }

    private ProjectileController SpawnProjectile()
    {
        GameObject prefab = Instantiate(projectilePrefab);
        prefab.transform.position = bulletSpawnTransform.position;
        prefab.transform.forward = bulletSpawnTransform.forward;
        return prefab.GetComponent<ProjectileController>();
    }
    #endregion
}

