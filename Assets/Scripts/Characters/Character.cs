﻿/*---------------------------------------------------------
 *
 * BulletsForBreakfast
 * Character.cs
 *
 * Created: 05.05.2019
 * By: Daniel D.
 *
 * ---------------------------------------------------------*/

using UnityEngine;

public class Character : MovingObject
{
    /*----------------
     * public fields
     * ---------------*/

    #region public fields

    [SerializeField]
    private int health = 3;
    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private float shootingDelay = 0.5f;
    [SerializeField]
    protected GameObject projectilePrefab;

    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public float ShootingDelay
    {
        get { return shootingDelay; }
        set { shootingDelay = value; }
    }

    #endregion

    /*-----------------
     * virtual methods
     * ----------------*/

    #region virtual methods

    protected virtual void HandleCharacterShoot() { }

    #endregion

    /*-----------------
     * public methods
     * ----------------*/

    #region public methods

    public void TakeDamage(int damage)
    {
        this.damage -= damage;
        if (this.damage <= 0)
            Destroy(gameObject);
    }

    #endregion

    /*-----------------
    * protected methods
    * ----------------*/

    #region Protected methods

    protected override void Update()
    {
        base.Update();
        HandleCharacterShoot();
    }

    #endregion

}
