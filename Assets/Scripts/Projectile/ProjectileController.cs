﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MovingObject
{
    public float lifeTime;
    public float movingSpeed;
    public GameObject shooterObject;
    public Vector3 shootingDirection;

    protected override void HandleMovement ()
    {
		if (lifeTime > 0.0f)
        {
            transform.position += shootingDirection * Time.deltaTime * movingSpeed;
        }
        else
        {
            DestroyImmediate(gameObject);
            return;
        }

        lifeTime -= Time.deltaTime;
	}

    void OnCollisionEnter(Collision otherObjectCollider)
    {
        var script = otherObjectCollider.gameObject.GetComponent<Character>();
        if (script != null && otherObjectCollider.gameObject != shooterObject)
        {
            script.TakeDamage(1);
            Destroy(gameObject);
        }

        if(otherObjectCollider.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }

        return;
    }
}
