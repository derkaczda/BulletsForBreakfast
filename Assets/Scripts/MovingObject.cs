﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    /*---------------------
     * public fields
     * --------------------*/
    #region public fields

    [SerializeField]
    private float movementSpeed = 5.0f;
    [SerializeField]
    private float rotationSpeed = 1.0f;


    public float MovementSpeed
    {
        get { return movementSpeed; }
        set { movementSpeed = value; }
    }

    public float RotationSpeed
    {
        get { return rotationSpeed; }
        set { rotationSpeed = value; }
    }

    #endregion
    /*-----------------
     * virtual methods
     * ----------------*/

    #region virtual methods

    protected virtual void HandleMovement() { }
    protected virtual void HandleRotation() { }

    #endregion

    /*-----------------
    * protected methods
    * ----------------*/

    #region Protected methods

    protected virtual void Update()
    {
        HandleMovement();
        HandleRotation();
    }

    #endregion
}
